/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.midtermalg;

import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class ALG_8 {
    
    public static void main(String[] args) {
        
       long start = System.nanoTime();
        
        Scanner kb = new Scanner(System.in);
        
        System.out.println("Input Size of Array:");
        int n = kb.nextInt();
        int[] A = new int[n];
        System.out.println("Input Array :" );
        for (int i=0;i<n;i++) {
            A[i] = kb.nextInt();
        }

        System.out.print("Reversed Array : [");
        for(n = A.length - 1; n >= 1; n--){
            System.out.print(A[n] + ", ");
        }
        System.out.print(A[n] + "]");
        
        System.out.println("");
        
        long stop = System.nanoTime();
        System.out.println("Running time=  "+(stop-start)*1E-9+" secs.");
    }
    
}
