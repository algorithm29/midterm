/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.midtermalg;

import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class ALG_11 {
    static int findSingle(int A[], int arr_size)
    {
        int res = A[0];
        for (int i = 1; i < arr_size; i++)
            res = res ^ A[i];
        return res;
    }
 
    
    public static void main (String[] args)
    {
        Scanner kb = new Scanner(System.in);
        System.out.println("Input Size of Array:");
        int n = kb.nextInt();
        int A[] = new int[n];
        for (int i=0;i<n;i++) {
            A[i] = kb.nextInt();
        }
        System.out.println("x is " +findSingle(A, n) + " ");
    }
}
